const { sequelize } = require('../../db/models');
exports.homePage = async (req, res, next) => {
	try {
		const { domain, user_type_id, user_id } = req.query;
		console.log('Log: exports.homePage -> user_id', user_id);
		console.log('Log: exports.homePage -> domain', domain);
		console.log('Log: exports.homePage -> user_type_id', user_type_id);
		if (!user_type_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid user_type_id',
			});
		}
		if (!domain) {
			return res.status(422).json({
				success: false,
				message: 'Invalid Domain',
			});
		}
		switch (parseInt(user_type_id)) {
			case 3:
				if (!user_id) {
					return res.status(422).json({
						success: false,
						message: 'Invalid user_id',
					});
				}
				await listJobs(domain, user_id, res);
				break;
			case 4:
				await listWorkers(domain, user_id, res);
				break;
			default:
				return res.status(422).json({
					success: false,
					message: 'Invalid user_type_id',
				});
				break;
		}
	} catch (error) {
		console.log('Log: exports.homePage -> error', error);
	}
};

listJobs = async (domain, user_id, res) => {
	try {
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			});
		}
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id = '${user_id}' and is_active='true'`
		);
		const companyData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_companydetail where id = '${UserData[0][0].company_id}'`
		);
		const workOrderData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder 
			where ((creation_timestamp > CURRENT_DATE-7 and status_id=5)or status_id!=5)
			and assigned_assignee_id ='${user_id}' and status_sys = true ORDER BY id DESC`
		);
		const workOrder = workOrderData[0];
		try {
			for (let index = 0; index < workOrder.length; index++) {
				const taskDetails = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertask where work_order_id ='${workOrder[index].id}'`
				);
				workOrder[index]['taskDetails'] = taskDetails[0];
				for (let j = 0; j < taskDetails[0].length; j++) {
					if (taskDetails[0][j].task_status_id != null) {
						const statusDetail_id = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${taskDetails[0][j].task_status_id}'`
						);
						workOrder[index]['taskDetails'][j]['statusDetail_id'] =
							statusDetail_id[0][0].unique_id;
					}
				}
				const workStatus = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderworkerstatus where wo_id = '${workOrder[index].id}' `
				);
				workOrder[index]['workStatus'] = workStatus[0];
				if (workOrder[index].milestone_id != null) {
					const milestoneDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projectmilestonedetail where id ='${workOrder[index].milestone_id}'`
					);
					workOrder[index]['milestoneDetail'] = milestoneDetail[0];
				}
				if (workOrder[index].project_id != null) {
					const projectDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projectdetail where id ='${workOrder[index].project_id}'`
					);
					workOrder[index]['projectDetail'] = projectDetail[0];
				}
				if (workOrder[index].supervisor_id != null) {
					const supervisorDetails = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where id ='${workOrder[index].supervisor_id}'`
					);
					workOrder[index]['supervisorDetails'] = supervisorDetails[0];
				}
				if (workOrder[index].task_id != null) {
					const taskDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projecttaskscheduledetail where id ='${workOrder[index].task_id}'`
					);
					workOrder[index]['taskDetail'] = taskDetail[0];
				}
				if (workOrder[index].wo_type_id) {
					const woTypeDetails = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertype where id ='${workOrder[index].wo_type_id}'`
					);
					workOrder[index]['woTypeDetails'] = woTypeDetails[0];
				}

				if (workOrder[index].assigned_group_id != null) {
					const assignedGroupDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_usergroups where id ='${workOrder[index].assigned_group_id}'`
					);
					// delete assignedGroupDetail[0]
					workOrder[index]['assignedGroupDetail'] = assignedGroupDetail[0];
				}
				if (workOrder[index].assigned_assignee_id != null) {
					const assignedAssignee = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workOrder[index].assigned_assignee_id}'`
					);
					workOrder[index]['assignedAssignee'] = assignedAssignee[0];
				}
				if (workOrder[index].created_by_id != null) {
					const createdByDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workOrder[index].created_by_id}'`
					);
					workOrder[index]['createdByDetail'] = createdByDetail[0];
				}
				if (workOrder[index].status_id) {
					const statusDetail_id = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${workOrder[index].status_id}'`
					);
					workOrder[index]['statusDetail_id'] = statusDetail_id[0][0].unique_id;
				}
				if (workOrder[index].wo_location_id) {
					const locationDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_worklocation where id ='${workOrder[index].wo_location_id}'`
					);
					workOrder[index]['locationDetail'] = locationDetail[0];
				}
				const UploadedData = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_attachmentdescription where work_order_id = '${workOrder[index].id}' `
				);
				workOrder[index]['UploadedData'] = UploadedData[0];
				for (let abc = 0; abc < UploadedData[0].length; abc++) {
					const UploadedAttachment = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderattachment where attachment_description_id = '${UploadedData[0][abc].id}' `
					);
					workOrder[index]['UploadedData'][abc]['attachments'] =
						UploadedAttachment[0];
				}
			}
		} catch (err) {
			console.log('Log: listWorkers -> error', err);
		}
		const locationDistance = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.general_app_settings_mapdefaults`
		);
		return res.json({
			success: true,
			isVerified: true,
			message: 'Job data fetched successfully',
			data: {
				locationDistance: locationDistance[0],
				workOrder,
				companyData: companyData[0][0],
			},
		});
	} catch (err) {
		console.log('err', err);
		return res.status(500).json({
			success: false,
			message: err.toString(),
		});
	}
};
listWorkers = async (domain, user_id, res) => {
	try {
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			});
		}
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id = '${user_id}' and is_active='true'`
		);
		const companyData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_companydetail where id = '${UserData[0][0].company_id}'`
		);
		const role = await sequelize.query(
			`SELECT id from ${domainData[0][0].schema_name}.user_access_roles where role='Worker'`
		);
		const workerData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where role_id='${role[0][0].id}' and status = true`
		);
		const workers = workerData[0];
		for (let i = 0; i < workers.length; i++) {
			let workerStatus = [];
			const skillData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_worker_skills where worker_id ='${workers[i].id}'`
			);
			let skills = [];
			for (let j = 0; j < skillData[0].length; j++) {
				let skill = await sequelize.query(
					`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${skillData[0][j].skills_id}'`
				);
				skills[j] = skill[0][0].name;
			}
			workers[i]['skills'] = skills;
			if (workers[i].user_worker_id !== null) {
				const currentLocation = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workercurrentlocation 
					where user_id ='${workers[i].user_worker_id}' and status = true`
				);
				workers[i]['currentLocation'] = currentLocation[0][0];
			}
			const roll = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_roles where id ='${workers[i].role_id}'`
			);
			workers[i]['roll'] = roll[0];
			const workerWorkData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder 
				where ((creation_timestamp > CURRENT_DATE-7 and status_id=5)or status_id!=5)
				and status_sys = true and assigned_assignee_id = ${workers[i].user_worker_id} ORDER BY id DESC`
			);
			workers[i]['workerWorkData'] = workerWorkData[0];
			workers[i]['workerWorkStatus'];
			let workerWorkStatus = [];
			for (let index = 0; index < workerWorkData[0].length; index++) {
				const taskDetails = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertask where work_order_id ='${workers[i]['workerWorkData'][index].id}'`
				);
				workers[i]['workerWorkData'][index]['taskDetails'] = taskDetails[0];
				for (let j = 0; j < taskDetails[0].length; j++) {
					if (taskDetails[0][j].task_status_id != null) {
						const statusDetail_id = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${taskDetails[0][j].task_status_id}'`
						);
						workers[i]['workerWorkData'][index]['taskDetails'][j][
							'statusDetail_id'
						] = statusDetail_id[0][0].unique_id;
					}
				}
				const workStatus = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderworkerstatus where wo_id = '${workers[i]['workerWorkData'][index].id}' `
				);
				workers[i]['workerWorkData'][index]['workStatus'] = workStatus[0];

				if (workers[i]['workerWorkData'][index].milestone_id != null) {
					const milestoneDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projectmilestonedetail where id ='${workers[i]['workerWorkData'][index].milestone_id}'`
					);
					workers[i]['workerWorkData'][index]['milestoneDetail'] =
						milestoneDetail[0];
				}
				if (workers[i]['workerWorkData'][index].project_id != null) {
					const projectDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projectdetail where id ='${workers[i]['workerWorkData'][index].project_id}'`
					);
					workers[i]['workerWorkData'][index]['projectDetail'] =
						projectDetail[0];
				}
				if (workers[i]['workerWorkData'][index].supervisor_id != null) {
					const supervisorDetails = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where id ='${workers[i]['workerWorkData'][index].supervisor_id}'`
					);
					workers[i]['workerWorkData'][index]['supervisorDetails'] =
						supervisorDetails[0];
				}
				if (workers[i]['workerWorkData'][index].task_id != null) {
					const taskDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.project_management_projecttaskscheduledetail where id ='${workers[i]['workerWorkData'][index].task_id}'`
					);
					workers[i]['workerWorkData'][index]['taskDetail'] = taskDetail[0];
				}
				if (workers[i]['workerWorkData'][index].wo_type_id) {
					const woTypeDetails = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertype where id ='${workers[i]['workerWorkData'][index].wo_type_id}'`
					);
					workers[i]['workerWorkData'][index]['woTypeDetails'] =
						woTypeDetails[0];
				}

				if (workers[i]['workerWorkData'][index].assigned_group_id != null) {
					const assignedGroupDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_usergroups where id ='${workers[i]['workerWorkData'][index].assigned_group_id}'`
					);
					// delete assignedGroupDetail[0]
					workers[i]['workerWorkData'][index]['assignedGroupDetail'] =
						assignedGroupDetail[0];
				}
				if (workers[i]['workerWorkData'][index].assigned_assignee_id != null) {
					const assignedAssignee = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workers[i]['workerWorkData'][index].assigned_assignee_id}'`
					);
					workers[i]['workerWorkData'][index]['assignedAssignee'] =
						assignedAssignee[0];
				}
				if (workers[i]['workerWorkData'][index].created_by_id != null) {
					const createdByDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workers[i]['workerWorkData'][index].created_by_id}'`
					);
					workers[i]['workerWorkData'][index]['createdByDetail'] =
						createdByDetail[0];
				}
				if (workers[i]['workerWorkData'][index].status_id) {
					const statusDetail_id = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${workers[i]['workerWorkData'][index].status_id}'`
					);
					workers[i]['workerWorkData'][index]['statusDetail_id'] =
						statusDetail_id[0][0].unique_id;
					workerWorkStatus.push(statusDetail_id[0][0].unique_id);
				}
				if (workers[i]['workerWorkData'][index].wo_location_id) {
					const locationDetail = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_worklocation where id ='${workers[i]['workerWorkData'][index].wo_location_id}'`
					);
					workers[i]['workerWorkData'][index]['locationDetail'] =
						locationDetail[0];
				}

				const UploadedData = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_attachmentdescription where work_order_id = '${workers[i]['workerWorkData'][index].id}' `
				);
				workers[i]['workerWorkData'][index]['UploadedData'] = UploadedData[0];
				for (let abc = 0; abc < UploadedData[0].length; abc++) {
					const UploadedAttachment = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderattachment where attachment_description_id = '${UploadedData[0][abc].id}' `
					);
					workers[i]['workerWorkData'][index]['UploadedData'][abc][
						'attachments'
					] = UploadedAttachment[0];
				}
			}
			if (workerWorkStatus.includes(1)) {
				workers[i]['workerWorkStatus'] = true;
			}
			if (workerWorkStatus.includes(5)) {
				workers[i]['workerWorkStatus'] = true;
			}
			if (workerWorkStatus.includes(2)) {
				workers[i]['workerWorkStatus'] = false;
			}
			if (workerWorkStatus.includes(3)) {
				workers[i]['workerWorkStatus'] = false;
			}
			if (workerWorkStatus.includes(4)) {
				workers[i]['workerWorkStatus'] = false;
			}
			if (workerWorkStatus.length == 0) {
				workers[i]['workerWorkStatus'] = null;
			}
		}
		let worker1 = workers.filter((worker) => {
			return worker.workerWorkStatus == true;
		});
		let worker2 = workers.filter((worker) => {
			return worker.workerWorkStatus == false;
		});
		let worker3 = workers.filter((worker) => {
			return worker.workerWorkStatus == null;
		});

		return res.json({
			success: true,
			isVerified: true,
			message: 'Worker data fetched successfully',
			data: {
				worker: [...worker1, ...worker2, ...worker3],
				companyData: companyData[0][0],
			},
		});
	} catch (err) {
		console.log('err', err);
		return res.status(500).json({
			success: false,
			message: err.toString(),
		});
	}
};
