const { sequelize } = require('../../db/models');
capitalizeFirstLetter = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1);
};
exports.search = async (req, res, next) => {
	try {
		let { domain, data = '' } = req.query;
		console.log('Log: exports.search -> data', data);
		console.log('Log: exports.search -> domain', domain);
		if (data == '') {
			return res.status(422).json({
				success: false,
				message: 'Search Query Could Not Be Null',
			});
		}
		console.log('Log: exports.search -> data', data);

		data = capitalizeFirstLetter(data.toLowerCase());
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			});
		}

		let skillData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_management_skills where name LIKE '${data}%' `
		);
		// if (skillData[0].length == 0) {
		// 	return res.status(422).json({
		// 		success: false,
		// 		message: 'No Worker Found',
		// 	})
		// }
		let wsList = [];
		for (let j = 0; j < skillData[0].length; j++) {
			let workerSkillData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_user_skills where skills_id ='${skillData[0][j].id}' `
			);
			for (let k = 0; k < workerSkillData[0].length; k++) {
				wsList.push(workerSkillData[0][k]);
			}
		}
		const role = await sequelize.query(
			`SELECT id from ${domainData[0][0].schema_name}.user_access_roles where role='Worker'`
		);
		let workers = [];
		const wlist = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where first_name LIKE '${data}%' and status = true and role_id='${role[0][0].id}'`
		);
		let workerByName = wlist[0];

		let workerBySkils = [];
		for (let i = 0; i < wsList.length; i++) {
			const worker = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where user_worker_id='${wsList[i].user_id}' and status = true and role_id='${role[0][0].id}'`
			);
			if (worker[0][0] != null) {
				workerBySkils.push(worker[0][0]);
			}
		}

		const wsDataList = [...workerByName, ...workerBySkils];
		for (let i = 0; i < wsDataList.length; i++) {
			const skillData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_user_skills where user_id ='${wsDataList[i].user_worker_id}'`
			);
			let skills = [];
			for (let j = 0; j < skillData[0].length; j++) {
				let skill = await sequelize.query(
					`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${skillData[0][j].skills_id}'`
				);
				skills[j] = skill[0][0].name;
			}
			wsDataList[i]['skills'] = skills;
			if (wsDataList[i].user_worker_id !== null) {
				const currentLocation = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workercurrentlocation 
					where user_id ='${wsDataList[i].user_worker_id}' and status = true`
				);
				wsDataList[i]['currentLocation'] = currentLocation[0][0];
			}
			if (wsDataList[i].role_id) {
				const roll = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.user_access_roles where id ='${wsDataList[i].role_id}'`
				);
				wsDataList[i]['roll'] = roll[0];
			}
			if (wsDataList[i].user_worker_id) {
				const workerWorkData = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder where 
					((creation_timestamp > CURRENT_DATE-7 and status_id=5)or status_id!=5) and status_sys = true and
					assigned_assignee_id = ${wsDataList[i].user_worker_id}`
				);
				wsDataList[i]['workerWorkData'] = workerWorkData[0];
				wsDataList[i]['workerWorkStatus'];
				let workerWorkStatus = [];
				for (let index = 0; index < workerWorkData[0].length; index++) {
					const taskDetails = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertask where work_order_id ='${wsDataList[i]['workerWorkData'][index].id}'`
					);
					wsDataList[i]['workerWorkData'][index]['taskDetails'] =
						taskDetails[0];
					for (let j = 0; j < taskDetails[0].length; j++) {
						if (taskDetails[0][j].task_status_id != null) {
							const statusDetail_id = await sequelize.query(
								`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${taskDetails[0][j].task_status_id}'`
							);
							wsDataList[i]['workerWorkData'][index]['taskDetails'][j][
								'statusDetail_id'
							] = statusDetail_id[0][0].unique_id;
						}
					}
					const workStatus = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderworkerstatus where wo_id = '${wsDataList[i]['workerWorkData'][index].id}' `
					);
					wsDataList[i]['workerWorkData'][index]['workStatus'] = workStatus[0];
					if (wsDataList[i]['workerWorkData'][index].milestone_id != null) {
						const milestoneDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.project_management_projectmilestonedetail where id ='${wsDataList[i]['workerWorkData'][index].milestone_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['milestoneDetail'] =
							milestoneDetail[0];
					}
					if (wsDataList[i]['workerWorkData'][index].project_id !== null) {
						const projectDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.project_management_projectdetail where id ='${wsDataList[i]['workerWorkData'][index].project_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['projectDetail'] =
							projectDetail[0];
					}
					if (wsDataList[i]['workerWorkData'][index].supervisor_id != null) {
						const supervisorDetails = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where id ='${wsDataList[i]['workerWorkData'][index].supervisor_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['supervisorDetails'] =
							supervisorDetails[0];
					}
					if (wsDataList[i]['workerWorkData'][index].task_id != null) {
						const taskDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.project_management_projecttaskscheduledetail where id ='${wsDataList[i]['workerWorkData'][index].task_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['taskDetail'] =
							taskDetail[0];
					}
					if (wsDataList[i]['workerWorkData'][index].wo_type_id) {
						const woTypeDetails = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertype where id ='${wsDataList[i]['workerWorkData'][index].wo_type_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['woTypeDetails'] =
							woTypeDetails[0];
					}
					if (
						wsDataList[i]['workerWorkData'][index].assigned_group_id != null
					) {
						const assignedGroupDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.user_access_usergroups where id ='${wsDataList[i]['workerWorkData'][index].assigned_group_id}'`
						);
						// delete assignedGroupDetail[0]
						wsDataList[i]['workerWorkData'][index]['assignedGroupDetail'] =
							assignedGroupDetail[0];
					}
					if (
						wsDataList[i]['workerWorkData'][index].assigned_assignee_id != null
					) {
						const assignedAssignee = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${wsDataList[i]['workerWorkData'][index].assigned_assignee_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['assignedAssignee'] =
							assignedAssignee[0];
					}
					if (wsDataList[i]['workerWorkData'][index].created_by_id != null) {
						const createdByDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${wsDataList[i]['workerWorkData'][index].created_by_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['createdByDetail'] =
							createdByDetail[0];
					}
					if (wsDataList[i]['workerWorkData'][index].status_id) {
						const statusDetail_id = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${wsDataList[i]['workerWorkData'][index].status_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['statusDetail_id'] =
							statusDetail_id[0][0].unique_id;
						workerWorkStatus.push(statusDetail_id[0][0].unique_id);
					}
					if (wsDataList[i]['workerWorkData'][index].wo_location_id) {
						const locationDetail = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_worklocation where id ='${wsDataList[i]['workerWorkData'][index].wo_location_id}'`
						);
						wsDataList[i]['workerWorkData'][index]['locationDetail'] =
							locationDetail[0];
					}
					const UploadedData = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_attachmentdescription where work_order_id = '${wsDataList[i]['workerWorkData'][index].id}' `
					);
					wsDataList[i]['workerWorkData'][index]['UploadedData'] =
						UploadedData[0];
					for (let abc = 0; abc < UploadedData[0].length; abc++) {
						const UploadedAttachment = await sequelize.query(
							`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderattachment where attachment_description_id = '${UploadedData[0][abc].id}' `
						);
						wsDataList[i]['workerWorkData'][index]['UploadedData'][abc][
							'attachments'
						] = UploadedAttachment[0];
					}
				}
				if (workerWorkStatus.includes(1)) {
					wsDataList[i]['workerWorkStatus'] = true;
				}
				if (workerWorkStatus.includes(5)) {
					wsDataList[i]['workerWorkStatus'] = true;
				}
				if (workerWorkStatus.includes(2)) {
					wsDataList[i]['workerWorkStatus'] = false;
				}
				if (workerWorkStatus.includes(3)) {
					wsDataList[i]['workerWorkStatus'] = false;
				}
				if (workerWorkStatus.includes(4)) {
					wsDataList[i]['workerWorkStatus'] = false;
				}
				if (workerWorkStatus.length == 0) {
					wsDataList[i]['workerWorkStatus'] = null;
				}
			}
		}

		const uniqueArray = wsDataList.filter((worker, index) => {
			const _worker = JSON.stringify(worker);
			return (
				index ===
				wsDataList.findIndex((obj) => {
					return JSON.stringify(obj) === _worker;
				})
			);
		});

		return res.json({
			success: true,
			isVerified: true,
			message: 'Search Api Fetched',
			data: { workers: uniqueArray },
		});
	} catch (error) {
		console.log('Log: exports.search -> error', error);
		return res.status(422).json({
			success: false,
			message: error.toString(),
		});
	}
};
