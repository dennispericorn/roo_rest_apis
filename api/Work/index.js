const express = require('express')

const router = express.Router()
const uploadMiddleware = require('../../middlewares/uploadMedia')
const Controller = require('./controller')

router.post('/accept', Controller.acceptWork)
router.post('/statusUpdate', Controller.statusUpdate)
router.post('/taskStatus', Controller.taskStatus)
router.post(
	'/uploadAttachments',
	uploadMiddleware,
	Controller.uploadAttachments
)
module.exports = router
