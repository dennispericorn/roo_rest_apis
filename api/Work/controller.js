const { sequelize } = require('../../db/models'),
	sendPushNotification = require('../../middlewares/pushnotification'),
	moment = require('moment'),
	unirest = require('unirest'),
	fs = require('fs')

exports.acceptWork = async (req, res, next) => {
	try {
		const { domain, user_type_id = 3, logged_by_id, wo_id } = req.body
		console.log('Log: exports.acceptWork -> domain', domain)
		console.log('Log: exports.acceptWork -> logged_by_id', logged_by_id)
		console.log('Log: exports.acceptWork -> wo_id', wo_id)

		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client 
			where schema_name = '${domain}'`
		)
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			})
		}
		if (!wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}
		if (!logged_by_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid logged_by_id',
			})
		}
		let wo_idData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder
			 where id = '${wo_id}' `
		)
		if (!wo_idData[0] || !wo_idData[0][0] || !wo_idData[0][0].wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}
		const isAccepted = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderworkerstatus 
			where wo_id = '${wo_id}' `
		)
		if (isAccepted[0].length != 0) {
			return res.status(422).json({
				success: false,
				message: 'Work not available',
			})
		}
		const status2 = await sequelize.query(
			`SELECT id from ${domainData[0][0].schema_name}.work_management_status where name = 'Assigned'`
		)
		const AcceptWork = await sequelize.query(
			`INSERT INTO ${
				domainData[0][0].schema_name
			}.work_management_workorderworkerstatus
			VALUES (DEFAULT, '${true}', '${logged_by_id}','${wo_id}')`
		)
		const UpdateStatus = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET status_id = '${status2[0][0].id}'
			 WHERE id = '${wo_id}'`
		)
		const UpdateDate = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET actual_start_date = current_date
			 WHERE id = '${wo_id}'`
		)
		const UpdateTime = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET actual_start_time = current_time
			 WHERE id = '${wo_id}'`
		)
		const AddWorkLog = await sequelize.query(
			`INSERT INTO ${domainData[0][0].schema_name}.work_management_workstatuslog
			VALUES (DEFAULT,current_timestamp,NULL,NULL ,'${logged_by_id}','${status2[0][0].id}',NULL,'${wo_id}',NULL,NULL,NULL,NULL,NULL,NULL)`
		)
		wo_idData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder
			where id = '${wo_id}' `
		)
		const statusDetail_id = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${wo_idData[0][0].status_id}'`
		)
		wo_idData[0][0]['statusDetail_id'] = statusDetail_id[0][0].unique_id
		const AddNotification = await sequelize.query(
			`INSERT INTO ${domainData[0][0].schema_name}.user_access_appnotification
			VALUES (DEFAULT,'Work Accepted', '${wo_id}',current_timestamp,NULL,'true','${logged_by_id}','${wo_idData[0][0].supervisor_id}')`
		)
		const supervisor_user = await sequelize.query(
			`SELECT user_worker_id from ${domainData[0][0].schema_name}.work_management_worker where id = '${wo_idData[0][0].supervisor_id}' and status='true'`
		)
		const supervisor_token = await sequelize.query(
			`SELECT notification_token from ${domainData[0][0].schema_name}.user_access_user where id = '${supervisor_user[0][0].user_worker_id}' and is_active='true'`
		)
		const worker = await sequelize.query(
			`SELECT first_name from ${domainData[0][0].schema_name}.user_access_user where id = '${wo_idData[0][0].assigned_assignee_id}' and is_active='true'`
		)
		console.log('Log: exports.acceptWork -> worker', worker)
		if (supervisor_token[0][0].notification_token != null) {
			const pushresponse = await sendPushNotification({
				registrationToken: supervisor_token[0][0].notification_token,
				message: `Work ${wo_idData[0][0].wo_id} accepted by ${worker[0][0].first_name}`,
			})
		}
		return res.json({
			success: true,
			isVerified: true,
			message: 'Work Accepted',
			data: {
				work: wo_idData[0][0],
			},
		})
	} catch (error) {
		console.log('Log: exports.homePage -> error', error)
		return res.status(422).json({
			success: false,
			message: error.toString(),
		})
	}
}
exports.statusUpdate = async (req, res, next) => {
	try {
		const {
			domain,
			user_type_id = 3,
			user_id,
			wo_id,
			prev_id,
			status_id,
		} = req.body
		console.log('Log: exports.statusUpdate -> prev_id', prev_id)
		console.log('Log: exports.statusUpdate -> domain', domain)
		console.log('Log: exports.statusUpdate -> user_id', user_id)
		console.log('Log: exports.statusUpdate -> wo_id', wo_id)
		console.log('Log: exports.statusUpdate -> status_id', status_id)
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		)
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			})
		}
		if (!wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}
		if (!user_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid user_id',
			})
		}
		if (!status_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid status_id',
			})
		}
		if (!prev_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid prev_id',
			})
		}
		let wo_idData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder where id = '${wo_id}' `
		)
		if (!wo_idData[0] || !wo_idData[0][0] || !wo_idData[0][0].wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}
		let workLogData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workstatuslog where work_id = '${wo_id}' ORDER BY id DESC`
		)
		const workLog = workLogData[0][0]
		const statusDetail = await sequelize.query(
			`SELECT id,name from ${domainData[0][0].schema_name}.work_management_status 
			where unique_id ='${status_id}'`
		)
		const status = statusDetail[0][0]
		const AcceptWork = await sequelize.query(
			`INSERT INTO ${domainData[0][0].schema_name}.work_management_workstatuslog
			VALUES (DEFAULT,current_timestamp,NULL,NULL ,'${user_id}','${status.id}','${workLog.changed_status_id}','${wo_id}',NULL,NULL,NULL,NULL,NULL,NULL)`
		)

		console.log('Log: exports.statusUpdate -> status', status)
		if (prev_id == 4) {
			const starttime = moment(workLogData[0][0].creation_timestamp)
			const workedTime = moment.duration(moment(new Date()).diff(starttime))
			let minutes = parseInt(workedTime.asMinutes())
			if (wo_idData[0][0].total_work_hours == null) {
				const UpdateTotalTime = await sequelize.query(
					`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET total_work_hours = ${minutes}
			 WHERE id = '${wo_id}'`
				)
			} else {
				minutes += parseInt(wo_idData[0][0].total_work_hours)
				const UpdateTotalTime = await sequelize.query(
					`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET total_work_hours = ${minutes}
			 WHERE id = '${wo_id}'`
				)
			}
		}
		const AddNotification = await sequelize.query(
			`INSERT INTO ${domainData[0][0].schema_name}.user_access_appnotification 
			VALUES (DEFAULT,'${status.name} ', '${wo_id}',current_timestamp,NULL,'true','${user_id}','${wo_idData[0][0].supervisor_id}')`
		)
		// need to add work update
		const UpdateWork = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.work_management_workorder 
			 SET status_id= '${status.id}' WHERE id = '${wo_id}'`
		)
		if (status_id == 5) {
			const UpdateDate = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET actual_finish_date = current_date
			 WHERE id = '${wo_id}'`
			)
			const UpdateTime = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_workorder
			 SET actual_finish_time = current_time
			 WHERE id = '${wo_id}'`
			)
		}
		wo_idData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder where id = '${wo_id}' `
		)
		const statusDetail_id = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${wo_idData[0][0].status_id}'`
		)

		//////////
		const supervisor_user = await sequelize.query(
			`SELECT user_worker_id from ${domainData[0][0].schema_name}.work_management_worker where id = '${wo_idData[0][0].supervisor_id}' and status='true'`
		)
		const supervisor_token = await sequelize.query(
			`SELECT notification_token from ${domainData[0][0].schema_name}.user_access_user where id = '${supervisor_user[0][0].user_worker_id}' and is_active='true'`
		)
		const worker = await sequelize.query(
			`SELECT first_name from ${domainData[0][0].schema_name}.user_access_user where id = '${wo_idData[0][0].assigned_assignee_id}' and is_active='true'`
		)

		console.log('Log: exports.statusUpdate -> worker', worker[0])
		if (supervisor_token[0][0].notification_token != null) {
			switch (status_id) {
				case 3:
					const pushpending = await sendPushNotification({
						registrationToken: supervisor_token[0][0].notification_token,
						message: `Work ${wo_idData[0][0].wo_id} status changed to pending by ${worker[0][0].first_name}`,
					})
					break
				case 4:
					const pushinprogress = await sendPushNotification({
						registrationToken: supervisor_token[0][0].notification_token,
						message: `Work ${wo_idData[0][0].wo_id} status changed to inprogress by ${worker[0][0].first_name}`,
					})
					break
				case 5:
					const pushcompleted = await sendPushNotification({
						registrationToken: supervisor_token[0][0].notification_token,
						message: `Work ${wo_idData[0][0].wo_id} status changed to completed by ${worker[0][0].first_name}`,
					})
					break
				case 12:
					const pushoffsite = await sendPushNotification({
						registrationToken: supervisor_token[0][0].notification_token,
						message: `Work ${wo_idData[0][0].wo_id} status changed to offsite by ${worker[0][0].first_name}`,
					})
					break

				default:
					break
			}
		}
		wo_idData[0][0]['statusDetail_id'] = statusDetail_id[0][0].unique_id
		return res.json({
			success: true,
			isVerified: true,
			message: 'Status Updated',
			data: {
				work: wo_idData[0][0],
			},
		})
	} catch (error) {
		console.log('Log: exports.statusUpdate -> error', error)
		return res.status(422).json({
			success: false,
			message: error.toString(),
		})
	}
}
exports.taskStatus = async (req, res, next) => {
	try {
		const {
			domain,
			user_type_id = 3,
			user_id = '',
			wo_id,
			task_id,
			status,
		} = req.body
		console.log('Log: exports.taskStatus -> status', status)
		console.log('Log: exports.taskStatus -> domain', domain)
		console.log('Log: exports.taskStatus -> user_id', user_id)
		console.log('Log: exports.taskStatus -> wo_id', wo_id)
		console.log('Log: exports.taskStatus -> task_id', task_id)
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		)
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			})
		}
		if (!wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}
		if (status == undefined) {
			return res.status(422).json({
				success: false,
				message: 'Invalid status',
			})
		}
		if (!task_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid task_id',
			})
		}
		let wo_idData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder where id = '${wo_id}' `
		)
		if (!wo_idData[0] || !wo_idData[0][0] || !wo_idData[0][0].wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid wo_id',
			})
		}

		// need to add tsk update
		if (status) {
			console.log('Making the Task status done')
			const taskDone = await sequelize.query(
				`SELECT id from ${domainData[0][0].schema_name}.work_management_status where name='Done'`
			)
			const UpdateTaskStatus = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_workordertask 
					SET task_status_id= '${taskDone[0][0].id}' WHERE id = '${task_id}'`
			)
		} else {
			console.log('Making the Task status not done')
			const taskNotDone = await sequelize.query(
				`SELECT id from ${domainData[0][0].schema_name}.work_management_status where name='Not Done'`
			)
			const UpdateTaskStatus = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_workordertask 
			 SET task_status_id= '${taskNotDone[0][0].id}' WHERE id = '${task_id}'`
			)
		}
		const taskDetails = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertask where work_order_id ='${wo_id}'`
		)
		let tasks = taskDetails[0]
		for (let j = 0; j < tasks.length; j++) {
			if (tasks[j].task_status_id != null) {
				const statusDetail_id = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${tasks[j].task_status_id}'`
				)
				tasks[j]['statusDetail_id'] = statusDetail_id[0][0].unique_id
			}
		}

		return res.json({
			success: true,
			isVerified: true,
			message: 'Status Updated',
			data: {
				tasks,
			},
		})
	} catch (error) {
		console.log('Log: exports.taskStatus -> error', error)
		return res.status(422).json({
			success: false,
			message: error.toString(),
		})
	}
}
exports.uploadAttachments = async (req, res, next) => {
	try {
		const { data } = req.body
		const dataObj = JSON.parse(data)
		console.log('Log: exports.UploadAttachments -> dataObj', dataObj)

		const domainData = await sequelize.query(
			`SELECT * from public.customer_management_client where schema_name = '${dataObj.domain}'`
		)
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			})
		}
		let wo_idData = await sequelize.query(
			`SELECT * from ${
				domainData[0][0].schema_name
			}.work_management_workorder where id = '${parseInt(dataObj.work_id)}' `
		)
		if (!wo_idData[0] || !wo_idData[0][0] || !wo_idData[0][0].wo_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid work_id',
			})
		}
		if (req.files.file || dataObj.description) {
			const resa = JSON.parse(
				await sendAttachments(
					req.files.file,
					domainData[0][0].domain_url,
					wo_idData[0][0].slug,
					wo_idData[0][0].id,
					dataObj.description
				)
			)
			if (req.files.file) {
				req.files.file.forEach((file) => {
					fs.unlinkSync(file.path)
				})
			}
			console.log('Log: exports.uploadAttachments -> resa', resa)
			if (!resa.status) {
				return res.status(502).json({
					success: false,
					message: 'Internal Server Error',
				})
			}
		}

		const Data = await sequelize.query(
			`SELECT * from ${
				domainData[0][0].schema_name
			}.work_management_attachmentdescription where work_order_id = '${parseInt(
				dataObj.work_id
			)}' `
		)
		let UploadedData = Data[0]
		for (let abc = 0; abc < UploadedData.length; abc++) {
			const UploadedAttachment = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderattachment where attachment_description_id = '${UploadedData[abc].id}' `
			)
			UploadedData[abc]['attachments'] = UploadedAttachment[0]
		}
		return res.json({
			success: true,
			message: 'Attachments Uploaded',
			data: UploadedData,
		})
	} catch (error) {
		console.log('Log: exports.statusUpdate -> error', error)
		return res.status(422).json({
			success: false,
			message: error.toString(),
		})
	}
}

sendAttachments = async (pfile = '', url, slug, wo_id, description = '') => {
	console.log('Log: sendAttachments -> pfile', pfile)
	return new Promise(async (resolve, rejects) => {
		try {
			const length = pfile.length
			console.log('Log: sendAttachments -> length', length)
			let req
			switch (length) {
				case 0:
					console.log('Log: sendAttachments -> length', length)
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				case 1:
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.attach('file', pfile[0].path)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				case 2:
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.attach('file', pfile[0].path)
						.attach('file', pfile[1].path)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				case 3:
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.attach('file', pfile[0].path)
						.attach('file', pfile[1].path)
						.attach('file', pfile[2].path)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				case 4:
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.attach('file', pfile[0].path)
						.attach('file', pfile[1].path)
						.attach('file', pfile[2].path)
						.attach('file', pfile[3].path)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				case 5:
					req = unirest('POST', `https://${url}/api/upload_wo_document/${slug}`)
						.attach('file', pfile[0].path)
						.attach('file', pfile[1].path)
						.attach('file', pfile[2].path)
						.attach('file', pfile[3].path)
						.attach('file', pfile[4].path)
						.field('wo_id', wo_id)
						.field('description', description)
						.end(function (res) {
							if (res.error) throw new Error(res.error)
							resolve(res.raw_body)
						})
					break
				default:
					break
			}
		} catch (error) {
			console.log('Log: sendImage -> error', error)
			rejects(error)
		}
	})
}
