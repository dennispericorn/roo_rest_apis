const { sequelize } = require( '../../db/models' );
const { generateAuthToken } = require( '../../middlewares/login' );
const hashers = require( 'node-django-hashers' );
const h = new hashers.PBKDF2PasswordHasher();
const Sequelize = require( 'sequelize' );

const sendPushNotification = require( '../../middlewares/pushnotification' );
exports.login = async ( req, res, next ) => {
	try {
		const { email, password, domain, registrationToken } = req.body;
		console.log( 'Log: exports.login -> registrationToken', registrationToken );
		console.log( 'Log: exports.login -> domain', domain );
		console.log( 'Log: exports.login -> email', email );
		if ( !email || !password || !domain || !registrationToken ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid Data',
			} );
		}
		options = {
			searchPath: 'public',
		};
		const domainData = await sequelize.query(
			`SELECT * from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		let updateToken = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.user_access_user
			SET notification_token = '${registrationToken}' where email = '${email}' `
		);
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where email = '${email}' and is_active='true'`
		);
		let User;
		if ( UserData[0][0] ) {
			User = UserData[0][0];

			const validPassword = await h.verify( password, User.password );
			console.log( 'Log: exports.login -> validPassword', validPassword );
			if ( !validPassword ) {
				return res.status( 422 ).json( {
					success: false,
					message: 'Invalid email or password.',
				} );
			}
		} else {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid email or password.',
			} );
		}
		const UserType = await sequelize.query(
			`SELECT name from ${domainData[0][0].schema_name}.user_management_usertype where id = '${User.user_type_id}'`
		);
		console.log( 'Log: exports.login -> UserType', UserType[0] );
		const UserSkills = await sequelize.query(
			`SELECT skills_id from ${domainData[0][0].schema_name}.user_access_user_skills where user_id = '${User.id}'`
		);
		console.log( 'Log: exports.login -> UserSkills', UserSkills[0] );
		// let skills = []
		for ( let index = 0; index < UserSkills[0].length; index++ ) {
			let skill = await sequelize.query(
				`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${UserSkills[0][index].skills_id}'`
			);
			UserSkills[0][index].skillName = skill[0][0].name;
		}

		const userId = await sequelize.query(
			`SELECT id from ${domainData[0][0].schema_name}.work_management_worker where user_worker_id = '${User.id}' and status='true'`
		);

		if ( !userId[0] || !userId[0][0] || !userId[0][0].id ) {
			User['id2'] = null;

		} else {
			User['id2'] = userId[0][0].id;
		}
		// if ( userId[0] || userId[0][0] || userId[0][0].id ) {

		User.skills = UserSkills[0];
		const token = generateAuthToken( User );
		delete User.password;
		// User['id2'] = userId[0][0].id;
		console.log( 'Log: exports.login -> User', User );

		return res.json( {
			success: true,
			isVerified: true,
			message: 'User data fetched successfully',
			data: {
				domainData: domainData[0][0],
				User,
				UserType: UserType[0][0],
				token,
			},
		} );
	} catch ( err ) {
		console.log( 'err', err );
		return res.status( 500 ).json( {
			success: false,
			message: err.toString(),
		} );
	}
};
