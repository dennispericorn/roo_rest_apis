const express = require('express')

const router = express.Router()

const Controller = require('./controller')

router.get('/', Controller.notification)
router.post('/status', Controller.notificationStatus)
module.exports = router
