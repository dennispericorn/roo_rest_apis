const { sequelize } = require( '../../db/models' );
exports.notification = async ( req, res, next ) => {
	try {
		const { domain, user_type_id, user_id } = req.query;
		console.log( 'Log: exports.notification -> user_type_id', user_type_id );
		console.log( 'Log: exports.notification -> domain', domain );
		console.log( 'Log: exports.notification -> user_id', user_id );

		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		if ( !user_type_id ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid user_type_id',
			} );
		}
		if ( user_id == null ) {
			return res.json( {
				success: true,
				message: 'Invalid user_id,its null now',
				data: {
					notification: []
				}
			} );
		}
		let userData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id = '${user_id}' `
		);
		if ( !userData[0] || !userData[0][0] || !userData[0][0].id ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid user_id',
			} );
		}
		let notificationData;
		if ( user_type_id == 3 ) {
			notificationData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_appnotification 
				where ((creation_timestamp > CURRENT_DATE-7 and status='false')or status!='false')
				 and user_id = '${user_id}' ORDER BY id DESC`
			);
		} else {
			notificationData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_appnotification
				where ((creation_timestamp > CURRENT_DATE-7 and status='false')or status!='false')
				 and supervisor_id = '${user_id}' ORDER BY id DESC`
			);
		}
		let notification = notificationData[0];
		for ( let index = 0; index < notification.length; index++ ) {
			const work = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder 
			where id = '${notificationData[0][index].notification}' `
			);
			notification[index]['work'] = work[0][0];
			const worker = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_worker 
			where user_worker_id = '${work[0][0].assigned_assignee_id}' `
			);
			notification[index]['worker'] = worker[0][0];
			const workStatus = await sequelize.query(
				`SELECT name from ${domainData[0][0].schema_name}.work_management_status 
			where id ='${work[0][0].status_id}'`
			);
			notification[index]['workStatus'] = workStatus[0][0];
		}

		return res.json( {
			success: true,
			isVerified: true,
			message: 'Notification Fetched',
			data: {
				notification,
			},
		} );
	} catch ( error ) {
		console.log( 'Log: exports.notification -> error', error );
		return res.status( 422 ).json( {
			success: false,
			message: error.toString(),
		} );
	}
};
exports.notificationStatus = async ( req, res, next ) => {
	try {
		const { domain, user_type_id, notification_id, user_id } = req.body;
		console.log( 'Log: exports.notificationStatus -> user_id', user_id );
		console.log(
			'Log: exports.notificationStatus -> notification_id',
			notification_id
		);
		console.log( 'Log: exports.notificationStatus -> user_type_id', user_type_id );
		console.log( 'Log: exports.notificationStatus -> domain', domain );

		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		if ( !user_type_id || !user_id || !notification_id ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid user_type_id or user_id or notification_id',
			} );
		}
		let notificationUpdate = await sequelize.query(
			`UPDATE ${domainData[0][0].schema_name}.user_access_appnotification
			SET viewed_timestamp = current_timestamp,status = 'false' where id = '${notification_id}' `
		);
		let notificationData;
		if ( user_type_id == 3 ) {
			notificationData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_appnotification 
				where ((creation_timestamp > CURRENT_DATE-7 and status='false')or status!='false') and user_id = '${user_id}' ORDER BY id DESC`
			);
		} else {
			notificationData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.user_access_appnotification 
				where ((creation_timestamp > CURRENT_DATE-7 and status='false')or status!='false') and supervisor_id = '${user_id}' ORDER BY id DESC`
			);
		}
		let notification = notificationData[0];
		for ( let index = 0; index < notification.length; index++ ) {
			const work = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder 
			where id = '${notificationData[0][index].notification}' `
			);
			notification[index]['work'] = work[0][0];
			const worker = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_worker 
			where user_worker_id = '${work[0][0].assigned_assignee_id}' `
			);
			notification[index]['worker'] = worker[0][0];
			const workStatus = await sequelize.query(
				`SELECT name from ${domainData[0][0].schema_name}.work_management_status 
			where id ='${work[0][0].status_id}'`
			);
			notification[index]['workStatus'] = workStatus[0][0];
		}

		return res.json( {
			success: true,
			isVerified: true,
			message: 'Notification Updated',
			data: {
				notification,
			},
		} );
	} catch ( error ) {
		console.log( 'Log: exports.notificationStatus -> error', error );
		return res.status( 422 ).json( {
			success: false,
			message: error.toString(),
		} );
	}
};
