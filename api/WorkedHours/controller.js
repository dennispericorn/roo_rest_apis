const { sequelize } = require('../../db/models');
exports.workedHours = async (req, res, next) => {
	try {
		const { domain, user_type_id = 3, user_id } = req.query;
		console.log('Log: exports.workedHours -> domain', domain);
		console.log('Log: exports.workedHours -> user_id', user_id);
		if (!user_id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid user_id',
			});
		}
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if (!domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name) {
			return res.status(422).json({
				success: false,
				message: 'Invalid domain',
			});
		}
		let userData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id = '${user_id}' `
		);
		if (!userData[0] || !userData[0][0] || !userData[0][0].id) {
			return res.status(422).json({
				success: false,
				message: 'Invalid user_id',
			});
		}
		let workData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.work_management_workorder where assigned_assignee_id = '${user_id}' and status_id = '5' `
		);
		const workLog = workData[0];
		for (let index = 0; index < workLog.length; index++) {
			const workStatus = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderworkerstatus where wo_id = '${workLog[index].id}' `
			);
			workLog[index]['workStatus'] = workStatus[0];
			if (workLog[index].total_work_hours != null) {
				const h = Math.floor(workLog[index].total_work_hours / 60);
				const m = workLog[index].total_work_hours % 60;
				workLog[index].total_work_hours = `${h} : ${m} Hrs`;
			}
			const taskDetails = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertask where work_order_id ='${workLog[index].id}'`
			);
			workLog[index]['taskDetails'] = taskDetails[0];
			for (let j = 0; j < taskDetails[0].length; j++) {
				if (taskDetails[0][j].task_status_id != null) {
					const statusDetail_id = await sequelize.query(
						`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${taskDetails[0][j].task_status_id}'`
					);
					workLog[index]['taskDetails'][j]['statusDetail_id'] =
						statusDetail_id[0][0].unique_id;
				}
			}
			if (workLog[index].milestone_id != null) {
				const milestoneDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.project_management_projectmilestonedetail where id ='${workLog[index].milestone_id}'`
				);
				workLog[index]['milestoneDetail'] = milestoneDetail[0];
			}
			if (workLog[index].project_id != null) {
				const projectDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.project_management_projectdetail where id ='${workLog[index].project_id}'`
				);
				workLog[index]['projectDetail'] = projectDetail[0];
			}
			if (workLog[index].supervisor_id != null) {
				const supervisorDetails = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_worker where id ='${workLog[index].supervisor_id}'`
				);
				workLog[index]['supervisorDetails'] = supervisorDetails[0];
			}
			if (workLog[index].task_id != null) {
				const taskDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.project_management_projecttaskscheduledetail where id ='${workLog[index].task_id}'`
				);
				workLog[index]['taskDetail'] = taskDetail[0];
			}
			if (workLog[index].wo_type_id) {
				const woTypeDetails = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workordertype where id ='${workLog[index].wo_type_id}'`
				);
				workLog[index]['woTypeDetails'] = woTypeDetails[0];
			}

			if (workLog[index].assigned_group_id != null) {
				const assignedGroupDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.user_access_usergroups where id ='${workLog[index].assigned_group_id}'`
				);
				// delete assignedGroupDetail[0]
				workLog[index]['assignedGroupDetail'] = assignedGroupDetail[0];
			}
			if (workLog[index].assigned_assignee_id != null) {
				const assignedAssignee = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workLog[index].assigned_assignee_id}'`
				);
				workLog[index]['assignedAssignee'] = assignedAssignee[0];
			}
			if (workLog[index].created_by_id != null) {
				const createdByDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id ='${workLog[index].created_by_id}'`
				);
				workLog[index]['createdByDetail'] = createdByDetail[0];
			}
			if (workLog[index].status_id) {
				const statusDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_status where id ='${workLog[index].status_id}'`
				);
				workLog[index]['statusDetail'] = statusDetail[0][0].id;
			}
			if (workLog[index].wo_location_id) {
				const locationDetail = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_worklocation where id ='${workLog[index].wo_location_id}'`
				);
				workLog[index]['locationDetail'] = locationDetail[0];
			}
			const UploadedData = await sequelize.query(
				`SELECT * from ${domainData[0][0].schema_name}.work_management_attachmentdescription where work_order_id = '${workLog[index].id}' `
			);
			workLog[index]['UploadedData'] = UploadedData[0];
			for (let abc = 0; abc < UploadedData[0].length; abc++) {
				const UploadedAttachment = await sequelize.query(
					`SELECT * from ${domainData[0][0].schema_name}.work_management_workorderattachment where attachment_description_id = '${UploadedData[0][abc].id}' `
				);
				workLog[index]['UploadedData'][abc]['attachments'] =
					UploadedAttachment[0];
			}
		}
		return res.json({
			success: true,
			isVerified: true,
			message: 'WorkLoad Fetched',
			data: {
				workLog,
			},
		});
	} catch (error) {
		console.log('Log: exports.workedHours -> error', error);
		return res.status(422).json({
			success: false,
			message: error.toString(),
		});
	}
};
