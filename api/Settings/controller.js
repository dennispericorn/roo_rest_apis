const { sequelize } = require( '../../db/models' );
const { generateAuthToken } = require( '../../middlewares/login' );
const hashers = require( 'node-django-hashers' );
const h = new hashers.PBKDF2PasswordHasher();
const Sequelize = require( 'sequelize' );
const unirest = require( 'unirest' );
const fs = require( 'fs' );

exports.changeData = async ( req, res, next ) => {
	try {
		console.log( 'Log: exports.changeData -> req.body', req.body );
		// const { data } = req.body
		const { email, first_name, last_name, phone_no, address, domain } = req.body;
		if ( !domain ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid Domain',
			} );
		}
		console.log( domain );
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		//First Name
		if ( first_name ) {
			const UpdateUser = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.user_access_user 
			 SET first_name= '${first_name}' WHERE email ='${email}'and is_active = 'true'`
			);
			const UpdateWorker = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_worker 
			 SET first_name= '${first_name}' WHERE email ='${email}'and status = 'true'`
			);
		}
		//Last Name
		if ( last_name ) {
			const UpdateUser = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.user_access_user 
			 SET last_name= '${last_name}' WHERE email ='${email}'and is_active = 'true'`
			);
			const UpdateWorker = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_worker 
			 SET last_name= '${last_name}' WHERE email ='${email}'and status = 'true'`
			);
		}
		// Phone No
		if ( phone_no ) {
			const UpdateUser = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.user_access_user 
			 SET phone_no= '${phone_no}' WHERE email ='${email}' and is_active = 'true'`
			);
			const UpdateWorker = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.work_management_worker 
			 SET phone_no= '${phone_no}' WHERE email ='${email}' and status = 'true'`
			);
		}
		//Address
		if ( address ) {
			const UpdateUser = await sequelize.query(
				`UPDATE ${domainData[0][0].schema_name}.user_access_user 
			 SET address= '${address}' WHERE email ='${email}' and is_active = 'true'`
			);
		}
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where email = '${email}'`
		);
		const User = UserData[0][0];
		const UserType = await sequelize.query(
			`SELECT name from ${domainData[0][0].schema_name}.user_management_usertype where id = '${User.user_type_id}'`
		);
		const UserSkills = await sequelize.query(
			`SELECT skills_id from ${domainData[0][0].schema_name}.user_access_user_skills where user_id = '${User.id}'`
		);
		console.log( 'Log: exports.login -> UserSkills', UserSkills[0] );
		// let skills = []
		for ( let index = 0; index < UserSkills[0].length; index++ ) {
			let skill = await sequelize.query(
				`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${UserSkills[0][index].skills_id}'`
			);
			UserSkills[0][index].skillName = skill[0][0].name;
		}
		User.skills = UserSkills[0];
		const token = generateAuthToken( User );
		delete User.password;
		return res.json( {
			success: true,
			isVerified: true,
			message: 'User data updated successfully',
			data: { User, UserType: UserType[0][0], token },
		} );
	} catch ( err ) {
		console.log( 'err', err );
		return res.status( 500 ).json( {
			success: false,
			message: err.toString(),
		} );
	}
};
exports.changePic = async ( req, res, next ) => {
	try {
		console.log( 'Log: exports.changePic -> req.body', req.body );
		const { data } = req.body;
		const { email, domain, user_id } = JSON.parse( data );
		console.log( 'Log: exports.changePic -> data', JSON.parse( data ) );
		if ( !domain ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid Domain',
			} );
		}
		console.log( domain );
		const domainData = await sequelize.query(
			`SELECT * from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		//Profile Picture
		const UserData0 = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where email = '${email}'`
		);
		console.log( 'Log: exports.changePic -> req.files.file', req.files );
		if ( req.files.file[0] ) {
			const resa = JSON.parse(
				await sendImage(
					req.files.file[0],
					domainData[0][0].domain_url,
					UserData0[0][0].id
				)
			);
			if ( resa.status ) {
				const imgpath = resa.url;
				const UpdateUser = await sequelize.query(
					`UPDATE ${domainData[0][0].schema_name}.user_access_user
					SET profile_picture='${imgpath}' WHERE email ='${email}' and is_active = 'true'`
				);
				const UpdateWorker = await sequelize.query(
					`UPDATE ${domainData[0][0].schema_name}.work_management_worker
						SET profile_image='${imgpath}' WHERE email ='${email}'and status = 'true'`
				);
				fs.unlinkSync( req.files.file[0].path );
			} else {
				return res.status( 500 ).json( {
					success: false,
					message: 'Internal Server Down',
				} );
			}
		}
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where email = '${email}'`
		);
		const User = UserData[0][0];
		console.log( 'Log: exports.changePic -> User', User );
		const UserType = await sequelize.query(
			`SELECT name from ${domainData[0][0].schema_name}.user_management_usertype where id = '${User.user_type_id}'`
		);
		const UserSkills = await sequelize.query(
			`SELECT skills_id from ${domainData[0][0].schema_name}.user_access_user_skills where user_id = '${User.id}'`
		);
		console.log( 'Log: exports.changePic -> UserSkills', UserSkills[0] );
		// let skills = []
		for ( let index = 0; index < UserSkills[0].length; index++ ) {
			let skill = await sequelize.query(
				`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${UserSkills[0][index].skills_id}'`
			);
			UserSkills[0][index].skillName = skill[0][0].name;
		}
		User.skills = UserSkills[0];
		const token = generateAuthToken( User );
		delete User.password;
		return res.json( {
			success: true,
			isVerified: true,
			message: 'Profile picture updated successfully',
			data: { User, UserType: UserType[0][0], token },
		} );
	} catch ( err ) {
		console.log( 'err', err );
		return res.status( 500 ).json( {
			success: false,
			message: err.toString(),
		} );
	}
};
exports.userData = async ( req, res, next ) => {
	try {
		const { user_id, domain } = req.query;
		console.log( 'Log: exports.userData -> domain', domain );
		console.log( 'Log: exports.userData -> user_id', user_id );
		if ( !user_id || !domain ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid Data',
			} );
		}
		const domainData = await sequelize.query(
			`SELECT schema_name from public.customer_management_client where schema_name = '${domain}'`
		);
		if ( !domainData[0] || !domainData[0][0] || !domainData[0][0].schema_name ) {
			return res.status( 422 ).json( {
				success: false,
				message: 'Invalid domain',
			} );
		}
		const UserData = await sequelize.query(
			`SELECT * from ${domainData[0][0].schema_name}.user_access_user where id = '${user_id}' and is_active='true'`
		);
		let User;
		if ( UserData[0][0] ) {
			User = UserData[0][0];
			const UserSkills = await sequelize.query(
				`SELECT skills_id from ${domainData[0][0].schema_name}.user_access_user_skills where user_id = '${User.id}'`
			);
			for ( let index = 0; index < UserSkills[0].length; index++ ) {
				let skill = await sequelize.query(
					`SELECT name from ${domainData[0][0].schema_name}.user_management_skills where id ='${UserSkills[0][index].skills_id}'`
				);
				UserSkills[0][index].skillName = skill[0][0].name;
			}
			const userId = await sequelize.query(
				`SELECT id from ${domainData[0][0].schema_name}.work_management_worker where user_worker_id = '${User.id}' and status='true'`
			);
			User.skills = UserSkills[0];

			delete User.password;

			if ( !userId[0] || !userId[0][0] || !userId[0][0].id ) {
				User['id2'] = null;
			} else {
				User['id2'] = userId[0][0].id;
			}
		} else {
			return res.status( 422 ).json( {
				success: false,
				message: 'user not exist',
			} );
		}
		const token = generateAuthToken( User );
		return res.json( {
			success: true,
			isVerified: true,
			message: 'User data fetched successfully',
			data: {
				User,
				token,
			},
		} );
	} catch ( err ) {
		console.log( 'err', err );
		return res.status( 500 ).json( {
			success: false,
			message: err.toString(),
		} );
	}
};

sendImage = async ( pfile, url, id ) => {
	return new Promise( ( resolve, rejects ) => {
		try {
			const req = unirest( 'POST', `https://${url}/api/update_profile_pic` )
				.attach( 'file', pfile.path )
				.field( 'user_id', id )
				.end( function ( res ) {
					if ( res.error ) throw new Error( res.error );
					resolve( res.raw_body );
				} );
		} catch ( error ) {
			console.log( 'Log: sendImage -> error', error );
		}
	} );
};
