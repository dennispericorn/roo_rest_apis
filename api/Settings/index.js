const express = require('express')
const router = express.Router()
const uploadMiddleware = require('../../middlewares/uploadMedia')
const controller = require('./controller')
router.get('/', controller.userData)
router.post('/changeData', controller.changeData)
router.post('/changePic', uploadMiddleware, controller.changePic)
module.exports = router
