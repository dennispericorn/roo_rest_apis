const login = require('./login');
const authorize = require('./authorize');
const dashboard = require('./dashboard');

module.exports = {
  ...login,
  ...authorize,
  ...dashboard
};
