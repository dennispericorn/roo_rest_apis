// const auth = require('../services/auth');
const express = require('express');
const request = require('request');

const router = express.Router();
const axios = require('axios');
const qs = require('querystring');
const circularJSON = require('circular-json');
const stripe = require('stripe')(
  'sk_test_By5qkaiHV37cCUAfboNfEEdT00nY4fLk19  '
);
router
  .post('/checkCode', async (req, res, next) => {
    try {
      const { code = undefined, email = undefined } = req.body;
      const requestBody = {
        grant_type: 'http://auth0.com/oauth/grant-type/passwordless/otp',
        client_id: 'ceQSteaW53G6eVvrzW8U1aqmwXG0HpKg',
        client_client_secret:
          'TKybGJFCgXMgV1wgG0StquBJd-Un0BJOCtBB3isPaFvTgpZaq2J-MgPugGyNW6O2',
        username: email,
        otp: code,
        realm: 'email',
        scope: 'openid profile email'
      };
      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };
      const response = await axios.post(
        'https://dev-mme-f619.auth0.com/oauth/token',
        qs.stringify(requestBody),
        config
      );

      const resp = JSON.parse(circularJSON.stringify(response));
      const access_token = resp.data.access_token;
      const refresh_token = resp.data.refresh_token;
      console.log('access_token', access_token);

      // SET UP COOKIE LOGIC refresh token, access token, expires in
      //

      if (access_token) {
        console.log('dfd');
        req.session.access_token = `Bearer ` + access_token;
        req.session.client_id = requestBody.client_id;
        console.log('req.session', req.session);
        // const access_token = response.access_token;
        // return res.render('admin/dashboard/index', { title: 'dashboard' });
        return res.json({
          STATUS: true,
          msg: 'Successful'
        });

        // return res.send({ response });
      } else {
        return res.json({
          STATUS: false,
          msg: 'Invalid OTP'
        });
        // console.log(response);
        // return res.send({ response });
      }

      // success

      // passward less strt

      // success

      // front end
      //Code end
    } catch (e) {
      console.log(e);
      // logger.error('/login :' + e);
      return res.json({
        STATUS: false,
        msg: e.response.data
      });
    }
  })
  // .post('/checkEmailExist', async (req, res, next) => {
  //   try {
  //     const { email = undefined } = req.body;
  //     console.log('req', req);
  //     const RESULT = await stripe.customers.list({ limit: 1, email });
  //     res.json({
  //       STATUS: true,
  //       MSG: 'User profile',
  //       RESULT
  //     });
  //   } catch (error) {
  //     logger.error('Admin#Dashboard  /login :' + error);

  //     res.locals.responseFormat = 'json';
  //     next(error);
  //   }
  // });
  .post('/checkEmailExist', async (req, res, next) => {
    try {
      const { email = undefined } = req.body;
      const result = await stripe.customers.list({ limit: 1, email });
      console.log('result', result.data);
      if (result.data.length) {
        console.log('result');
        const options = {
          method: 'POST',
          url: 'https://dev-mme-f619.auth0.com/passwordless/start',
          headers: { 'content-type': 'application/json' },
          body: {
            client_id: 'ceQSteaW53G6eVvrzW8U1aqmwXG0HpKg',
            client_secret:
              'TKybGJFCgXMgV1wgG0StquBJd-Un0BJOCtBB3isPaFvTgpZaq2J-MgPugGyNW6O2',
            connection: 'email',
            email: email,
            send: 'code',
            authParams: {
              scope: 'offline_access'
            }
          },
          json: true
        };
        console.log('options', options);
        const response = await request(options);
        console.log('response----', response);
        if (response) {
          return res.json({
            STATUS: true,
            MSG: 'We have send you a message, please check your email.'
          });
        }

        // success

        // passward less strt

        // success

        // front end
      } else {
        return res.json({
          STATUS: false,
          MSG: 'No User Exist'
        });
      }
    } catch (error) {
      // logger.error('Admin#Dashboard  /login :' + error);
      console.log(error);
      // res.locals.responseFormat = 'json';
      next(error);
    }
  });

module.exports = router;
