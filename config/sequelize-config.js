const {database} = require('./index');

module.exports = {
	development: {
		...database
	},
	test: {},
	production: {}
};
