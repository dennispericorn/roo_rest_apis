require('dotenv-flow').config()

module.exports = {
	server: {
		HOSTNAME: process.env.HOSTNAME,
		PORT: process.env.PORT,
	},
	database: {
		username: process.env.DATABASE_USERNAME,
		password: process.env.DATABASE_PASSWORD,
		database: process.env.DATABASE_NAME,
		host: process.env.DATABASE_HOST,
		dialect: process.env.DATABASE_DIALECT,
		port: process.env.DATABASE_PORT,
	},
	mailer: {
		port: 587,
		host: 'smtp.gmail.com',
		user: 'aswathi.k@spericorn.com',
		password: 'aashnairknr',
	},

	jwt: {
		secretkey: '7bvGPLyeRND7FXOWNs2WzKsF1',
		expires: 2400,
	},
	firebaseConfig: {
		apiKey: '',
		authDomain: '',
		databaseURL: '',
		projectId: '',
		storageBucket: '',
		messagingSenderId: '',
		appId: '',
		apiAuthorizationKey: '',
	},
	firebaseCloudMessaging: {
		dbUrl: 'https://roowms.firebaseio.com',
	},
}
