const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const db = require('./db/models')
const socket = require('./middlewares/socketApi')
const { updateLoction } = require('./functions/UpdateLoction')

// Load environment variables from .env
const dotenv = require('dotenv')
// Load Passport
// const passport = require('passport');
// const Auth0Strategy = require('passport-auth0');
// const apiRouter = require('./routes/api');
dotenv.config()

const app = express()
app.use(cors())
// force: true will drop the table if it already exists
db.sequelize
	.sync({ force: false })
	.then(() => console.log('Successfully synced Models with DB'))
	.catch((err) => console.error('Sync Error: ', err))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.static(path.join(__dirname, 'public')))

// routes
// app.use('/api', apiRouter);
app.use(express.json({ limit: '300mb' }))
app.use(express.urlencoded({ limit: '300mb', extended: true }))
app.use(cookieParser())

// app.use('/', adminRouter); //admin
app.all('/api/*', [require('./middlewares/auth')])
app.use('/', require('./routes/api'))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404))
})
// error handler
app.use(function (err, req, res, next) {
	// console.log(res)
	// console.log(err)
	if (res.locals.responseFormat === 'json') {
		return res.status(err.status || 422).send({
			STATUS: false,
			MSG: err.message,
			RESULT: '',
		})
	} else {
		res.status(err.status || 500)
		// console.log(err)
		return res.json({
			message: 'Page Not Found !',
			status: '404',
		})
	}
})

socket.io.on('connection', (socket) => {
	socket.on('new-location', (data) => {
		updateLoction(data)
	})
})

module.exports = app
