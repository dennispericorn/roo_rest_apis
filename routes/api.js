const express = require('express')

const router = express.Router()

router.get('/', (req, res, next) => res.send('Welcome to Roo'))
router.use('/auth', require('../api/Auth'))
router.use('/api/home', require('../api/Home'))
router.use('/api/settings', require('../api/Settings'))
router.use('/api/work', require('../api/Work'))
router.use('/api/workedHours', require('../api/WorkedHours'))
router.use('/api/notification', require('../api/Notification'))
router.use('/api/search', require('../api/Search'))
// router.use('/api/item', require('../api/Item'))
// router.use('/api/order', require('../api/Order'))

module.exports = router
