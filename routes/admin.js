const express = require('express')
const url = require('url')
const router = express.Router()
const pageHandler = require('../controllers/admin/pageHandler')
const axios = require('axios')
const qs = require('querystring')
const circularJSON = require('circular-json')
const authorizeCheck = require('../middlewares/authorizeUser')
console.log('authorizeCheck', authorizeCheck)

const ignorePaths = [
	'/login',
	'/changePassword',
	'/uploads',
	'/assets',
	'/authorize',
]

/*-------------------------------------------APIs- AJAX POST 
REQUESTS---------------------------------------------*/
// router.use('/', require('../controllers/admin/auth'));

async function authChecker(req, res, next) {
	try {
		if (!ignorePaths.some((p) => req.path === '/' || req.path.startsWith(p))) {
			//New code starts here
			const requestBody = {
				client_id: req.session.client_id,
			}
			const config = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization': req.session.access_token,
				},
			}
			const response = await axios.post(
				' https://dev-mme-f619.auth0.com/userinfo',
				qs.stringify(requestBody),
				config
			)

			const resp = JSON.parse(circularJSON.stringify(response))
			console.log('resp0000000000', resp.data.email_verified)

			if (resp.data.email_verified) {
				console.log('............./if')
				return next()

				// return res.send({ response });
			} else {
				console.log('............./error')
				return res.redirect('/login')
				// return next('Error');
				// console.log(response);
				// return res.send({ response });
			}
		}
		return next()
	} catch (e) {
		console.log('axioserror', e)
		return res.redirect('/login')
	}
}

function middleWares(req, res, next) {
	try {
		const urlParsed = url.parse(req.originalUrl)
		const pathParts = urlParsed.pathname.split('/')
		const parent = pathParts[1]
		const child = pathParts[2] ? pathParts[2] : null
		console.log('urlParsed', urlParsed.pathname)
		// console.log('pathParts', pathParts);
		// console.log('parent', parent);
		// console.log('child', child);
		// console.log('pageHandler[parent][child]', pageHandler['admin']);
		if (child) {
			console.log('...................>')
			if (
				pageHandler[parent][child] &&
				pageHandler[parent][child].call &&
				pageHandler[parent][child].apply
			) {
				return pageHandler[parent][child](req, res, next)
			}
		}
		if (
			pageHandler[parent] &&
			pageHandler[parent].call &&
			pageHandler[parent].apply
		) {
			return pageHandler[parent](req, res, next)
		}

		return next()
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

/*-------------------------------------------Pages---------------------------------------------*/
/* GET home page. */
router.get('/', (req, res, next) => {
	return res.redirect('/login')
})
router.get('/logout', (req, res, next) => {
	req.session.access_token = ''
	req.session.client_id = ''
	return res.redirect('/login')
})
router.get('/*', authChecker, middleWares)

module.exports = router
