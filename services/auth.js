const users = [
  {
    id: 1,
    username: 'tiller@mailinator',
    password: 'tiller123',
    firstName: 'Tiller',
    lastName: 'Admin'
  }
];
// let {
//   user,
//   department,
//   user_type,
//   downloads_analysis,
//   country,
//   state,
//   city,
//   user_submission_data
// } = require('../../db/models');
// let validationhelper = require('../helpers/validationHelper');
// let settings = require("../../config");
// let moment = require('moment');
// let _ = require('lodash');
// const rememberDays = 7;
const stripe = require('stripe')(process.env.STRIPE_KEY);
module.exports = {
  login: async function(param) {
    let userData = await users.findOne({
      // attributes:['content'],
      where: {
        email: param.email,
        // password: param.password,
        is_deleted: 0
      }
    });
    if (!userData) {
      return null;
    }
    const validPassword = await users.verifyPassword(
      param.password,
      userData.password
    );
    if (validPassword) {
      return userData;
    }
    return null;
    // return userData;
  },
  //To check weather mail exist in the user table
  checkUserMail: async function(param) {
    let response = [];
    let getUserMail = await user.findOne({
      // attributes:['content'],
      where: {
        email: param.email,
        is_deleted: 0
      }
    });
    if (getUserMail) {
      response['status'] = 'true';
    } else {
      response['status'] = 'false';
    }
    return getUserMail;
  },
  //To get super admin mail
  getSuperAdminMail: async function(param) {
    let getSuperAdminMail = await user.findAll({
      // attributes:['content'],
      where: {
        type_id: param
      }
    });

    return getSuperAdminMail;
  },
  //To save forgot password token on user table
  saveToken: async function(param) {
    let response = [];
    const result = await user.update(
      { token: param.token },
      { where: { id: param.user_id } }
    );
    response['status'] = true;
    return response;
  },

  //To change password of local admin in user table
  updateUserPassword: async function(param) {
    let response = [];
    if (user.validatePassword(param.password)) {
      param.hash = await user.hashPassword(param.password);
      const result = await user.update(
        { password: param.hash },
        { where: { token: param.token } }
      );
      if ((result[0] = 1)) {
        response['status'] = true;
      } else {
        response['statusMsg'] = 'Failed, Please try again';
        response['status'] = false;
      }
      // let userData = await user.create(params);
      // return {status: true, message: "User created", result: userData};
    } else {
      response['statusMsg'] =
        'Password must contain at least one digit and must be at least 8 characters long.';
      response['status'] = false;
    }
    // const result = await user.update(
    //     {password: param.password},
    //     {where: {token: param.token}}
    // )
    // if (result[0] = 1) {
    //     response['status'] = true;
    // } else {
    //     response['statusMsg'] = "Failed, Please try again";
    //     response['status'] = false;
    // }
    return response;
  },
  //Get user data using token
  getTokenData: async function(param) {
    let userData = await user.findOne({
      attributes: ['email', 'id', 'fname'],
      where: {
        token: param,
        is_deleted: 0
      }
    });
    if (!userData) {
      throw createError(404, 'Not found!');
    }
    return userData;
  },
  getUserData: async function(userId) {
    let userData = await user.findOne({
      where: {
        id: userId
      }
    });

    return userData;
  },
  getAdminUserData: async function(params) {
    let response = [];
    let whereObj = {};
    whereObj = {
      is_deleted: 0,
      type_id: 2
    };
    if (params.department_id) {
      whereObj = {
        is_deleted: 0,
        department_id: params.department_id,
        type_id: 2
      };
    }
    let userData = await user.findAll({
      include: [
        {
          model: department,
          attributes: ['id', 'name']
        }
      ],
      where: [whereObj],
      offset: parseInt((params.pageNo - 1) * params.limit),
      limit: parseInt(params.limit),
      subQuery: false
    });
    let resultCount = await user.count({
      where: [whereObj]
    });

    if (userData.length > 0) {
      response['userData'] = userData;
      response['totalUsersCount'] = resultCount;

      return response;
    }
    return false;
  },
  getProfile: async function(params) {
    let userData = await user.findOne({
      attributes: [
        'id',
        'fname',
        'lname',
        'email',
        'city',
        'state',
        'createdAt'
      ],
      where: {
        id: params.id,
        type_id: 3
      }
    });

    return userData;
  },
  getProfileForBadge: async function(params) {
    let userData = await user.findOne({
      attributes: [
        'id',
        'fname',
        'lname',
        'email',
        'city',
        'state',
        'createdAt'
      ],
      where: {
        id: params.id,
        type_id: { $in: [3, 4] }
      }
    });

    return userData;
  },
  getAdminProfile: async function(params) {
    const userData = await user.findOne({
      include: [
        {
          model: department
        }
      ],
      where: {
        id: params.userId,
        type_id: 2
      }
    });

    return userData ? userData.dataValues : {};
  },
  saveProfile: async function(params) {
    let response = {};
    let paramData = {
      type_id: 3,
      fname: params.fname,
      lname: params.lname,
      email: params.email,
      city: params.city,
      state: params.state
    };
    if (!params.userId) {
      response.status = false;
      response.message = 'User id required';
      return response;
    }
    let paramCheck = await validationhelper.checkObject(paramData);
    if (paramCheck) {
      //let userData = await user.create(paramData);
      let userData = await user.update(paramData, {
        where: [{ id: params.userId }]
      });
      response.status = true;
      //response.userData = userData ;
      return response;
    }
    response.status = false;
    response.message = 'Failed to save';
    return response;
  },
  fetchUserByEmail: async (filter = {}) => {
    // is_subscribed, department_id
    const userDetailsByEmail = await user.findOne({
      attributes: [
        'id',
        'fname',
        'lname',
        'email',
        'phone',
        'city',
        'state',
        'uuId'
      ],
      where: {
        ...filter,
        is_deleted: 0,
        type_id: 3
      },
      include: [
        {
          model: department,
          attributes: [
            'id',
            'name',
            'email',
            'country_id',
            'state_id',
            'city_id'
          ],
          include: [
            {
              model: country,
              attributes: ['id', 'name']
            },
            {
              model: state,
              attributes: ['id', 'name', 'code']
            },
            {
              model: city,
              attributes: ['id', 'name']
            }
          ]
        }
      ],
      order: [['id', 'DESC']]
    });
    return userDetailsByEmail ? userDetailsByEmail.dataValues : null;
  },
  createAdmin: async function(params) {
    let checkEmail = await user.findOne({
      where: {
        email: params.email,
        is_deleted: 0,
        type_id: 2
      }
    });
    if (!checkEmail) {
      if (user.validatePassword(params.password)) {
        params.password = await user.hashPassword(params.password);
        let userData = await user.create(params);
        return { status: true, message: 'User created', result: userData };
      }
      return {
        status: false,
        message:
          'Password must contain at least one digit and must be at least 8 characters long.'
      };
    }
    return { status: false, message: 'Email already exists', result: '' };
  },
  updateAdmin: async function(params) {
    let paramObj = {
      id: params.id,
      fname: params.fname,
      email: params.email
      // password: params.password
    };
    if (params.password) {
      if (user.validatePassword(params.password)) {
        paramObj.password = await user.hashPassword(params.password);
      } else {
        return {
          status: false,
          message:
            'Password must contain at least one digit and must be at least 8 characters long.'
        };
      }
      // return {
      //     status: false,
      //     message: "Password must contain at least one digit and must be at least 8 characters long."
      // };
    }
    // if (params.password == '') {
    //     paramObj = {
    //         id: params.id,
    //         fname: params.fname,
    //         email: params.email
    //     };
    // } else {
    // if (user.validatePassword(params.password)) {
    //     paramObj.password = await user.hashPassword(params.password);
    // }
    // // return {
    // //     status: false,
    // //     message: "Password must contain at least one digit and must be at least 8 characters long."
    // // };
    // }

    await user.update(paramObj, {
      where: [{ id: paramObj.id }]
    });
    return {
      status: true,
      message: 'Success'
    };
  },
  softDeleteAdmin: async function(params) {
    await user.update({ is_deleted: 1 }, { where: [{ id: params.userId }] });
    return true;
  },
  userList: async (page = 1, limit = 10, filter = {}, options = {}) => {
    const { pagination } = options;
    let paginate = {};
    if (pagination) {
      page = page ? Math.floor(parseInt(page)) : 1;
      const itemsPerPage = limit ? Math.floor(parseInt(limit)) : 10;
      if (page < 1) page = 1;
      const offset = (page - 1) * itemsPerPage;
      paginate = {
        limit: itemsPerPage,
        offset: offset
      };
    }
    const userInfos = await user.findAll({
      where: {
        ...filter,
        is_deleted: 0
      },
      include: [
        {
          model: user_type,
          attributes: ['id', 'name']
        }
      ],
      ...paginate,
      order: [['createdAt', 'DESC']]
    });

    return userInfos && userInfos.length
      ? userInfos.map(p => p.dataValues)
      : [];
  },
  userCount: async (filter = {}) => {
    const userCount = await user.count({
      where: {
        ...filter,
        is_deleted: 0
      }
    });

    return userCount;
  },
  userById: async id => {
    const userInfo = await user.findOne({
      where: {
        id,
        is_deleted: 0
      },
      include: [
        {
          model: user_type,
          attributes: ['id', 'name']
        },
        {
          model: department,
          attributes: ['id', 'name']
        }
      ]
    });
    if (!userInfo || !userInfo.dataValues) {
      throw createError(404, 'User not found!');
    }
    return userInfo.dataValues;
  },
  //To save the department details and udid from app side
  saveDepartment: async function(param) {
    let response = {};
    let department_list = [];
    let departmentCount = 0;
    // user checking ends here#uses uuId for first user & uses userId for changing profile
    let checkFilter = {};
    if (param.createStatus == 1) {
      checkFilter = {
        uuId: param.uuId,
        is_deleted: 0
      };
    } else {
      checkFilter = {
        id: param.userId,
        is_deleted: 0
      };
    }
    let checkUser = await user.findOne({
      where: checkFilter
    });
    // user checking ends here
    let paramData = {
      type_id: 4, //Anonymous user - By default
      uuId: param.uuId,
      department_id: param.departmentId
    };
    if (param.departmentId) {
      let getCount = await department.findOne({
        attributes: ['id', 'department_count'],
        where: {
          id: param.departmentId,
          is_deleted: 0
        }
      });

      departmentCount = getCount.department_count + 1;
      let updateConter = await department.update(
        { department_count: departmentCount },
        { where: { id: param.departmentId } }
      );

      // for showing chart in admin start
      const statisticsPayload = {
        uu_id: param.uuId,
        department_id: param.departmentId
      };
      const downloads_analysis_data = await downloads_analysis.findOne({
        where: statisticsPayload
      });
      if (downloads_analysis_data) {
        await downloads_analysis.update(statisticsPayload, {
          where: statisticsPayload
        });
      } else {
        await downloads_analysis.create(statisticsPayload);
      }
      // for showing chart in admin end
    }

    //Validation check
    let paramCheck = await validationhelper.checkObject(paramData);

    if (!checkUser || param.createStatus == 1) {
      //param.createStatus==1#In department/email API if email is not found then new user is created with uuID
      if (paramCheck) {
        department_list.push(param.departmentId);
        paramData.department_list = JSON.stringify(department_list);
        let userData = await user.create(paramData);

        response.status = true;
        response.id = userData.id;
        return response;
      } else {
        response.status = false;
        response.message = 'Please check all fields';
        return response;
      }
    } else {
      if (paramCheck) {
        let list = JSON.parse(checkUser.dataValues.department_list);
        var arrayLength = list.length;
        for (var i = 0; i < arrayLength; i++) {
          department_list.push(list[i]);
        }
        department_list.push(param.departmentId);

        let result = await user.update(
          {
            department_id: param.departmentId,
            department_list: JSON.stringify(department_list)
          },
          { where: { uuId: param.uuId } }
        );
        response.status = true;
        response.id = checkUser.id;
        return response;
      } else {
        response.status = false;
        response.message = 'Please check all fields';
        return response;
      }
    }
  },

  //To get the user badge list
  getProfileBadge: async function(param) {
    let response = {};
    let officerFlag = 0;
    let sergeantFlag = 0;
    let detectiveFlag = 0;
    let captianFlag = 0;
    let chiefFlag = 0;
    let neighbourFlag = 0;
    let defaultPath = {
      image: ''
    };
    response.badgeList = [];
    let image = [];
    let hostname = settings.server.hostName;
    let port = settings.server.port;
    //To get the user details
    let userData = await user.findOne({
      where: {
        id: param.userid,
        type_id: { $in: [3, 4] },
        is_deleted: 0
      },
      include: [
        {
          model: department,
          attributes: ['id', 'name', 'createdAt']
        }
      ]
    });
    if (userData) {
      //To check wheather the user submitted tip,commend or feedback.
      let isUserDataSubmitted = await user_submission_data.count({
        where: {
          user_id: userData.dataValues.id,
          is_deleted: 0
        }
      });
      response.chief = false;
      let downloadedDate = userData.createdAt;
      let today = new Date();
      let neighbour_downloadedDate = moment(downloadedDate, 'DD-MM-YYYY')
        .add(4, 'days')
        .format();
      let officer_downloadedDate = moment(downloadedDate, 'DD-MM-YYYY')
        .add(6, 'days')
        .format();
      let sergeant_downloadedDate = moment(downloadedDate, 'DD-MM-YYYY')
        .add(39, 'days')
        .format();
      let captian_downloadedDate = moment(downloadedDate, 'DD-MM-YYYY')
        .add(59, 'days')
        .format();
      let cheif_downloadedDate = moment(downloadedDate, 'DD-MM-YYYY')
        .add(179, 'days')
        .format();
      // let path = "http://" + hostname + ":" + port + "/assets/admin/images/badges/";
      const env = process.env.NODE_ENV;
      let path =
        `http://${settings.server.hostName}:${settings.server.port}` +
        '/assets/admin/images/badges/';
      if (env == 'production') {
        path =
          `https://${settings.server.hostName}` +
          '/assets/admin/images/badges/';
      }

      if (moment(today).isAfter(neighbour_downloadedDate, 'day')) {
        neighbourFlag = 1;
        response.badgeList.push({
          type: 'neighbor',
          image: path + 'neighbour.png'
        });
      } else {
        response.badgeList.push({ type: 'neighbor', ...defaultPath });
      }
      if (moment(today).isAfter(officer_downloadedDate, 'day')) {
        officerFlag = 1;
        response.badgeList.push({
          type: 'officer',
          image: path + 'officer.png'
        });
      } else {
        response.badgeList.push({ type: 'officer', ...defaultPath });
      }
      if (moment(today).isAfter(sergeant_downloadedDate, 'day')) {
        sergeantFlag = 1;
        response.badgeList.push({
          type: 'sergeant',
          image: path + 'sergeant.png'
        });
      } else {
        response.badgeList.push({ type: 'sergeant', ...defaultPath });
      }
      if (
        isUserDataSubmitted >= 1 &&
        neighbourFlag == 1 &&
        officerFlag == 1 &&
        sergeantFlag == 1
      ) {
        detectiveFlag = 1;
        response.badgeList.push({
          type: 'detective',
          image: path + 'detective.png'
        });
      } else {
        response.badgeList.push({ type: 'detective', ...defaultPath });
      }
      if (
        moment(today).isAfter(captian_downloadedDate, 'day') &&
        officerFlag == 1 &&
        detectiveFlag == 1
      ) {
        captianFlag = 1;
        response.badgeList.push({
          type: 'captain',
          image: path + 'captian.png'
        });
      } else {
        response.badgeList.push({ type: 'captain', ...defaultPath });
      }
      if (
        moment(today).isAfter(cheif_downloadedDate, 'day') &&
        officerFlag == 1 &&
        detectiveFlag == 1 &&
        sergeantFlag == 1 &&
        captianFlag == 1
      ) {
        chiefFlag = 1;
        response.badgeList.push({ type: 'chief', image: path + 'chief.png' });
        response.chief = true;
      } else {
        response.badgeList.push({ type: 'chief', ...defaultPath });
      }
      // let badgeList = response.badgeList
      //     let count=5;
      //     let n=count-badgeList.length

      //     for(let i=0;i<n;i++){
      //         badgeList.push(defaultPath);
      //     }

      // response.badgeList = _.orderBy(response.badgeList, ['image'], ['desc']);
      response.status = true;
      return response;
    } else {
      response.status = false;
      return response;
    }
  },
  //To get the user push notification subscriber count
  userSubscriberCount: async function(filter = {}) {
    const userCount = await user.count({
      where: {
        is_subscribed: 1,
        is_deleted: 0,
        ...filter
      }
    });

    return userCount;
  },
  editRememberTwoFactor: async (userId, remember = true) => {
    const currDate = moment()
      .utc()
      .format();
    const rememberDate = remember
      ? moment(currDate)
          .add(rememberDays, 'days')
          .utc()
          .format()
      : null;
    await user.update(
      {
        '2fa_remember_date': rememberDate
      },
      {
        where: {
          id: userId
        }
      }
    );

    return true;
  },
  removeUserDepartment: async (filter = {}) => {
    await user.update(
      { department_id: null },
      {
        where: {
          ...filter
        }
      }
    );

    return true;
  },
  getAllUsersByDeptId: async (filter = {}) => {
    filter = {
      // department_id:filter.deptId,
      type_id: { $in: [3, 4] }
    };
    let users = await user.findAll({
      where: {
        ...filter
      }
    });
    return users;
  }
};
