/* jshint indent: 2 */
'use strict'
module.exports = function (sequelize, DataTypes) {
	const customer_management_client = sequelize.define(
		'customer_management_client',
		{
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.INTEGER,
			},
			domain_url: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			schema_name: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: false,
			},
			paid_until: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			on_trial: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
			},
			created_on: {
				type: DataTypes.DATE,
				allowNull: false,
				defaultValue: DataTypes.NOW(),
			},
		},
		{
			schema: 'public',
		}
	)
	return customer_management_client
}
