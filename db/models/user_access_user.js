/* jshint indent: 2 */
'use strict'
module.exports = function (sequelize, DataTypes) {
	const user_access_user = sequelize.define(
		'user_access_user',
		{
			slug: {
				allowNull: false,
				type: DataTypes.STRING,
			},
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.INTEGER,
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			last_login: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			first_name: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			last_name: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			username: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			is_staff: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
			},
			is_superuser: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
			},
			is_active: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
			},
			is_verified: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
			},
			creation_timestamp: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			parent_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			company_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			address: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			city: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			country: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			department_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			dob: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			emergency_contact: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			employee_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			phone_no: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			state: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			street_name: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			street_no: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			user_type_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			created_by_id: {
				type: DataTypes.INTEGER,
				allowNull: true,
			},
			authentication_key: {
				type: DataTypes.STRING,
				allowNull: true,
			},
		},
		{}
	)
	return user_access_user
}
