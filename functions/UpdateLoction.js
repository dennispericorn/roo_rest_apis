const { sequelize } = require('../db/models')

updateLoction = async (data) => {
	console.log(`Socket call from worker: ${data.user_id}`, data)
	const { domain, longitude, latitude, user_id, locationName } = data
	const isDataLoaded = await sequelize.query(
		`SELECT * from ${domain}.work_management_workercurrentlocation 
			where user_id ='${user_id}' and status = true`
	)
	if (Object.keys(isDataLoaded[0]).length === 0) {
		console.log('New Location')
		const AddLocation = await sequelize.query(
			`INSERT INTO ${domain}.work_management_workercurrentlocation
			VALUES (DEFAULT, '${latitude}','${longitude}','null','null',current_timestamp, '${true}','${user_id}','${locationName}')`
		)
	} else {
		console.log('UPDATE Location')
		const UpdateLocation = await sequelize.query(
			`UPDATE ${domain}.work_management_workercurrentlocation
			 SET latitude = '${latitude}',
			 longitude = '${longitude}',
			 creation_timestamp=current_timestamp,
			 location_name = '${locationName}'
			 WHERE user_id = '${user_id}'`
		)
	}
}
module.exports = {
	updateLoction,
}
