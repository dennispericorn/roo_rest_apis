const express = require('express');

const router = express.Router();
const axios = require('axios');
const qs = require('querystring');
const circularJSON = require('circular-json');
router.post('/authChecker', async (req, res, next) => {
  try {
    const { code = undefined, email = undefined } = req.body;
    const requestBody = {
      client_id: req.session.client_id
    };
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: req.session.access_token
      }
    };
    const response = await axios.post(
      ' https://dev-mme-f619.auth0.com/userinfo',
      qs.stringify(requestBody),
      config
    );

    const resp = JSON.parse(circularJSON.stringify(response));
    console.log('resp0000000000', resp);

    if (resp.email_verified) {
      console.log('ok');
      // const access_token = response.access_token;
      // return res.render('admin/dashboard/index', { title: 'dashboard' });
      return res.json({
        STATUS: true,
        msg: 'Successful'
      });

      // return res.send({ response });
    } else {
      return res.json({
        STATUS: false,
        msg: 'failed'
      });
      // console.log(response);
      // return res.send({ response });
    }

    // success

    // passward less strt

    // success

    // front end
    //Code end
  } catch (e) {
    console.log(e);
    // logger.error('/login :' + e);
    return res.json({
      STATUS: false,
      msg: e.response.data
    });
  }
});

module.exports = router;
