const admin = require('firebase-admin')
const serviceAccount = require('../config/roo-firebase-admin.json')

const {
	firebaseCloudMessaging: { dbUrl },
} = require('../config')

const firebaseApp = admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: dbUrl,
})
const notification_options = {
	priority: 'high',
	timeToLive: 60 * 60 * 24,
}
async function sendPushNotification(payload) {
	console.log('Log: sendPushNotification -> payload', payload)
	const {
		registrationToken,
		title = 'RooWMS',
		message,
		notificationId = '',
	} = payload
	const data = {
		notification: {
			title: title,
			sound: 'default',
			body: message,
			// image: image,
		},
		data: {
			// urlImageString: image,
			title: title,
			body: message,
			notificationId: notificationId,
		},
	}
	return new Promise((resolve, reject) => {
		firebaseApp
			.messaging()
			.sendToDevice(registrationToken, data, notification_options)
			.then((response) => {
				console.log('Log: sendPushNotification -> response', response)
				resolve(response)
			})
			.catch((error) => {
				reject(error)
			})
	})
}

module.exports = sendPushNotification
