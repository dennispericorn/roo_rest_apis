/* eslint-disable no-else-return */
const multer = require('multer')
const { unlinkFiles } = require('../modules/fileOperations')
const DIR = './public/uploads/attachments/'
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, DIR)
	},
	filename: (req, file, cb) => {
		const fileName = file.originalname.toLowerCase().split(' ').join('-')
		cb(null, fileName)
	},
})
const upload = multer({
	storage,
	fileFilter: (req, file, cb) => {
		const type = file.mimetype.substring(0, file.mimetype.lastIndexOf('/'))
		if (type == 'image' || type == 'application' || type == 'video') {
			cb(null, true)
		} else {
			cb(null, false)
			return cb(new Error('Only image, application and video format allowed!'))
		}
	},
}).fields([
	{
		name: 'file',
		maxCount: 5,
	},
])
// custom error handling middleware for file upload
module.exports = async (req, res, next) => {
	const files = []
	try {
		upload(req, res, (err) => {
			console.log('files--->', req.files)
			next()
		})
	} catch (error) {
		console.log(error)
		unlinkFiles(files)
		res.locals['responseFormat'] = 'json'
		return next(error)
	}
}
