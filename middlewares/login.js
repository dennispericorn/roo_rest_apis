/* jshint indent: 2 */
'use strict'
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {
	jwt: { secretkey, expires },
} = require('../config')
// const hashers = require('node-django-hashers')
// const h = new hashers.PBKDF2PasswordHasher()

// const validatePassword = function (pass) {
// 	return /^(?=.*\d).{8,}$/.test(pass)
// }
// const hashPassword = async function (pass) {
// 	const saltRounds = 10
// 	return await bcrypt.hash(pass, saltRounds)
// }
// const verifyPassword = async function (pass, hash) {
// 	console.log('Log: pass', pass)
// 	console.log('Log: hash', hash)
// 	return await bcrypt.compare(pass, hash)
// }
const expireIn = (numDays) => {
	const dateObj = new Date()
	return dateObj.setDate(dateObj.getDate() + numDays)
}
const generateAuthToken = function (userData) {
	let expiresIn = expireIn(1)
	return jwt.sign(
		{
			id: userData.id,
			username: userData.username,
		},
		secretkey,
		{ expiresIn }
	)
}
module.exports = {
	generateAuthToken,
}
