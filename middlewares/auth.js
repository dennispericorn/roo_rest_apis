const jwt = require('jsonwebtoken')

const {
	jwt: { secretkey },
} = require('../config')

module.exports = (req, res, next) => {
	// console.log('Log: req', req)
	try {
		if (!req.header('Authorization')) {
			return res.status(401).json({
				status: false,
				message: 'Access denied. No token provided.',
			})
		}
		const token = req.header('Authorization').replace('Bearer ', '')
		// if (replace == undefined) {
		// 	return res.status(401).json({
		// 		status: false,
		// 		message: 'Access denied. No token provided.',
		// 	})
		// }
		console.log('token--------', token)
		if (!token || token == undefined || token == '') {
			return res.status(401).json({
				status: false,
				message: 'Access denied. No token provided.',
			})
		}
		const decoded = jwt.verify(token, secretkey)
		if (decoded.exp * 1000 <= Date.now()) {
			return res.status(401).json({
				status: false,
				message: 'Token Expired',
			})
		}
		req.user = decoded
		// return res.status(400).json({
		// 	user: req.user,
		// 	message: 'Token Expired',
		// });
		return next()
	} catch (ex) {
		console.log(ex)
		return res.status(401).json({
			success: false,
			message: 'Invalid Token.',
		})
	}
}
