const nodeMailer = require("nodemailer");
const ejs = require("ejs");
const {mailer} = require("../../config");
const {readFilePromise} = require("../fileOperations");

const send = function (mailOptions) {
    return nodeMailer
        .createTransport({
            host: mailer.host,
            port: mailer.port,
            secure: false,
            auth: {
                user: mailer.user,
                pass: mailer.password,
            },
            tls: {
                rejectUnauthorized: false,
            },
        })
        .sendMail(mailOptions);
};

module.exports = {
    welcomeMail: async (renderData = {}, to = []) => {
        const welcomeMailEjs = await readFilePromise("modules/templates/adminWelcome.ejs");
        const renderedTemplate = ejs.render(welcomeMailEjs, renderData, {});
        const mailOptions = {
            from: mailer.user,
            to: to,
            cc: mailer.user,
            subject: "Groc First Mail",
            html: renderedTemplate,
        };
        const response = await send(mailOptions);
        return response;
    },
    userSubmissionNotify: async (renderData = {}, to = [], subject = 'Grocery Notification') => {
        const notificationMailEjs = await readFilePromise("modules/templates/userSubmissionNotificationMail.ejs");
        const renderedTemplate = ejs.render(notificationMailEjs, renderData, {});
        const mailOptions = {
            from: mailer.user,
            to: to,
            cc: mailer.user,
            subject: subject,
            html: renderedTemplate,
        };
        const response = await send(mailOptions);
        return response;
    },
    forgotPasswordMail: async (renderData = {}, to = [], subject = 'Grocery Notification') => {
        const forgotPasswordMailEjs = await readFilePromise("modules/templates/forgotPasswordMail.ejs");
        const renderedTemplate = ejs.render(forgotPasswordMailEjs, renderData, {});
        const mailOptions = {
            from: mailer.user,
            to: to,
            cc: mailer.user,
            subject: subject,
            html: renderedTemplate,
        };
        const response = await send(mailOptions);
        return response;
    },
    changePasswordNotify: async (renderData = {}, to = [], subject = 'MyPD Notification') => {
        const changePasswordNotificationEjs = await readFilePromise("modules/templates/changePasswordNotification.ejs");
        const renderedTemplate = ejs.render(changePasswordNotificationEjs, renderData, {});
        const mailOptions = {
            from: mailer.user,
            to: to,
            cc: mailer.user,
            subject: subject,
            html: renderedTemplate,
        };
        const response = await send(mailOptions);
        return response;
    },
};
